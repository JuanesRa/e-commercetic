import { Injectable } from '@angular/core';
import { HttpClient ,HttpHeaders,HttpParams   } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class GlobalServicesService {
  //URL_BASE="http://localhost:8080";
  constructor(private httpClient: HttpClient) { }
  /*getProductos(){
    return this.httpClient.get(this.URL_BASE + '/producto' );
  }*/
  getProductos(): Observable<any>{
    return this.httpClient.get('http://localhost:8080/producto')
  }
}
