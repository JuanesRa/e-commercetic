import { Component, OnInit } from '@angular/core';
import { GlobalServicesService } from 'src/app/service/global-services.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  Productos!:any;
  constructor(public globalServices:GlobalServicesService) { }

  ngOnInit(): void {
    this.setProductos()
  }
  setProductos(){
    this.globalServices.getProductos().subscribe((data:any)=>{
      this.Productos = data
    })
  }
}
